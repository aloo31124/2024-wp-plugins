<?php
/* 
 * Plugin Name: test contact form
 * Description: 測試用
 * Version: 1.0.0
 * Author: loulou
 * Text Domain: test-contact-form
 
 * Requires at least: 6.4
 * Requires PHP: 7.4
 */

echo "顯示前台測試";
defined( 'ABSPATH' ) || exit;

class TestContactForm {

    public function __construct()
    {
        // 客製 post 表
        add_action("init", array($this, "create_custom_post_type"));

        // 新增 [前台] assets 腳本(js, css, etc)
        add_action("wp_enqueue_scripts", array($this, "load_assets"));

        // 新增 短代碼 short code
        add_shortcode("test-contact-form", array($this, "load_shortcode"));

        // load 送出 js
        add_action("wp_footer", array($this, "load_acripts"));

        // 註冊 rest api
        add_action("rest_api_init", array($this, "register_rest_api"));
        
    }

    public function create_custom_post_type() {
        //echo "<script>alert('js測試! yoyo!')</script>";
        $args = array(
            "public" => true,
            "has_archive" => true,
            "supports" => array("title"),
            "exclude_form_search" => true,
            "publicly_queryable" => false,
            "capability" => "manage_options",
            "labels" => array(
                "name" => "test contact form",
                "singular_name" => "test contact form yo",
            ),
            "menu_icon" => "dashicons-media-text",
        );
        register_post_type("test_contact_form", $args);
    }
    

    // 新增 [前台] assets 腳本(js, css, etc)
    public function load_assets() {
        // 前台 引入 css
        wp_enqueue_style(
            "test-contact-form",
            plugin_dir_url(__FILE__) . "/css/test-contact-form.css",
            array(),
            1,
            "all"
        );

        // 前台 引入 js
        wp_enqueue_script(            
            "test-contact-form",
            plugin_dir_url(__FILE__) . "/js/test-contact-form.js",
            array("jquery"),
            1,
            true
        );
    }

/* 
    public function load_shortcode() {
        return "yo 短代碼運作成功拉拉拉阿!";
    }
 */

    // 短代碼 崁入 php
    public function load_shortcode() 
    {?>

        <div>
            <h1>寄送信箱</h1>
            <p>請填寫如下內容</p>

            <form class="simple-contact-form">
                <input name="user-name" type="text" placeholder="輸入姓名">
                <input name="user-mail" type="email" placeholder="信箱">
                <input type="tel" placeholder="電話">
                <textarea placeholder="輸入資訊"></textarea>
                <button type="submit">送出</button>
            </form>
        </div>

    <?php }

    public function load_acripts() 
    {?>
        <script>
            //alert("load_acripts ");

            var nonce = "<?php echo wp_create_nonce("wp_rest"); ?>";

            (function($){
                $(".simple-contact-form").submit(function(event) {
                    //alert("送出開始!! !");
                    event.preventDefault();
                    var form = $(this).serialize();
                    console.log(form);

                    $.ajax({
                        method: "post",
                        url: "<?php echo get_rest_url(null, "test-contact-form/v1/send-email"); ?>",
                        headers: {"X-WP-Nonce": nonce},
                        data: form
                    });
                });
            })(jQuery)

        </script>

    <?php }

    public function register_rest_api() {
        register_rest_route(
            "test-contact-form/v1",
            "send-email",
            array(
                "method" => "POST",
                "callback" => array($this, "handle_contact_form")
            )
        );
    }

    public function handle_contact_form($data)
    {
        $headers = $data -> get_headers();
        $params = $data -> get_params();
        $nonce = $headers["x_wp_nonce"][0];
        //$nonce = 1234878;
        if(!wp_verify_nonce($nonce, "wp_rest")) {
            return new WP_REST_Response("資訊不正確", 422);
        }
        $post_id = wp_insert_post([
            "post_type" => "test-contact-form",
            "post_title" => "Contact enquiry",
            "post_status" => "publish"
        ]);
    }

}

new TestContactForm;
