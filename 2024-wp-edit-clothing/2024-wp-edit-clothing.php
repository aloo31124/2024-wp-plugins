<?php
/* 
 * Plugin Name: 2024-wp-edit-clothing
 * Description: 米粒 衣服編輯功能, 測試用
 * Version: 1.0.0
 * Author: loulou
 * Text Domain: 2024-wp-edit-clothing
 
 * Requires at least: 6.4
 * Requires PHP: 7.4
 */

//echo "衣服編輯, 顯示前台測試";
defined( 'ABSPATH' ) || exit;

class EditClothing {

    public function __construct()
    {
        // 客製 後台 post 表
        add_action("init", array($this, "create_custom_post_type"));
        add_action("init", array($this, "img_upload"));

        // 新增 [前台] assets 腳本(js, css, etc)
        add_action("wp_enqueue_scripts", array($this, "load_assets"));

        // 新增 短代碼 short code
        add_shortcode("edit-clothing", array($this, "load_shortcode"));

        // load 送出 js
        add_action("wp_footer", array($this, "load_acripts"));

    }

    public function create_custom_post_type() {
        //echo "<script>alert('js測試! yoyo!')</script>";

        $args = array(
            "public" => true,
            "has_archive" => true,
            "supports" => array("title"),
            "exclude_form_search" => true,
            "publicly_queryable" => false,
            "capability" => "manage_options",
            "labels" => array(
                "name" => "Edit Clothing", // 後台標籤名稱
                "singular_name" => "test contact form yo",
            ),
            "menu_icon" => "dashicons-media-text",
        );
        register_post_type("test_contact_form2", $args);
    }
    

    // 新增 [前台] assets 腳本(js, css, etc)
    public function load_assets() {
        // 前台 引入 css
        wp_enqueue_style(
            "2024-wp-edit-clothing",
            plugin_dir_url(__FILE__) . "/css/edit-clothing.css",
            array(),
            1,
            "all"
        );

        // 前台 引入 js
        wp_enqueue_script(            
            "2024-wp-edit-clothing",
            plugin_dir_url(__FILE__) . "/js/edit-clothing.js",
            array("jquery"),
            1,
            true
        );
    }

/* 
    public function load_shortcode() {
        return "yo 短代碼運作成功拉拉拉阿!";
    }
 */



    public function img_upload() {
        echo "--上船測試 4--";
        if(isset( $_POST["bwp_upload"])) {
            //echo "<pre>";print_r(($_FILES));"</pre>";
            //echo "<pre>";print_r(($_POST));"</pre>";

            require_once(ABSPATH . "wp-admin" . '/includes/file.php');

            //$file = $_FILES["myfile"];
            $overrides = array( 'test_form' => false);
            $upload_file = wp_handle_upload($_FILES['myfile'], $overrides);
            if(!is_wp_error($upload_file)) {
                //echo "<pre>"; print_r($upload_file);"</pre>";
                update_option("my_upload_image_url", $upload_file["url"]);
            }
            
        }

        /* 
        $file_url = get_option("my_upload_image_url");
        if(!empty($file_url)){
            echo "<img src='" .esc_url($file_url) ."' />";            
        }
         */
    }

    // 短代碼 崁入 php
    public function load_shortcode() 
    {?>

        <div>
            <h1>衣服編輯</h1>
            <form name="uploader" method="post" enctype="multipart/form-data">
                <input type="file" name="myfile">
                <input type="hidden" name="bwp_upload" value="1">
                <input type="submit" name="upload_file">
            </form>
            <div class="edit-clothing-container">
                <!-- <img class="edit-clothing-img" src="http://localhost/wordpress/wp-content/plugins/2024-wp-edit-clothing/img/smile.png"> -->
                <?php

                    $file_url = get_option("my_upload_image_url");
                    if(!empty($file_url)){
                        echo "<img class='edit-clothing-img' src='" .esc_url($file_url) ."' />";            
                    } 
                    else {
                        echo "<img class='edit-clothing-img' src='http://localhost/wordpress/wp-content/plugins/2024-wp-edit-clothing/img/smile.png' />";                                    
                    }

                ?>
            </div>
        </div>
    <?php }

    public function load_acripts() 
    {
        
        
        ?>
        <script>
            let isDragging = false;
            let startX, startY, initialLeft, initialTop;

            const clothingImg = document.querySelector(".edit-clothing-img");


            // 鼠标按下事件
            clothingImg.addEventListener('mousedown', (e) => {
                isDragging = true;
                startX = e.clientX;
                startY = e.clientY;
                initialLeft = parseInt(clothingImg.style.left || 0, 10);
                initialTop = parseInt(clothingImg.style.top || 0, 10);
                document.addEventListener('mousemove', onMouseMove);
                document.addEventListener('mouseup', onMouseUp);
            });

            // 鼠标移动事件
            function onMouseMove(e) {
                if (isDragging) {
                    const dx = e.clientX - startX;
                    const dy = e.clientY - startY;
                    clothingImg.style.left = `${initialLeft + dx}px`;
                    clothingImg.style.top = `${initialTop + dy}px`;
                }
            }

            // 鼠标释放事件
            function onMouseUp() {
                isDragging = false;
                document.removeEventListener('mousemove', onMouseMove);
                document.removeEventListener('mouseup', onMouseUp);
            }
        </script>

    <?php }

}

new EditClothing;
